Rails.application.routes.draw do
  resources :users, :defaults => { :format => 'json' }
  resources :todos, :defaults => { :format => 'json' }
end
