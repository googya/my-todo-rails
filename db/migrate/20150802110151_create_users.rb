class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.string :address_line
      t.string :post_code
      t.string :country

      t.timestamps null: false
    end
  end
end
