json.users do |json|
  @users.each  do |user|
    json.extract! user, :id, :first_name, :last_name, :address_line, :post_code, :country
    json.url user_url(user, format: :json)
  end
end
